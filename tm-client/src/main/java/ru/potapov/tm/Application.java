package ru.potapov.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

/**
 * Application
 * v 1.0.7
 */

@Getter
@Setter
public final class Application {
    @NotNull final private String version = "1.0.7";


    public Application() {
        try {
            SeContainerInitializer.newInstance()
                    .addPackages(Application.class).initialize()
                    .select(Bootstrap.class).get().init();
//            SeContainerInitializer.newInstance()
//                    .addPackages(Application.class).initialize()
//                    .select(AppTest.class).get().init();
        }catch (Exception e){ e.printStackTrace(); }
    }

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        new Application();
    }
}
