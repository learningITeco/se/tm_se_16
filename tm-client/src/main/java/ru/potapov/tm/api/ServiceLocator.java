package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import java.text.SimpleDateFormat;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull ITerminalService getTerminalService();
    @NotNull IUserService getUserService();
    @NotNull ISessionService getSessionService();
    @NotNull SimpleDateFormat getFt();
}
