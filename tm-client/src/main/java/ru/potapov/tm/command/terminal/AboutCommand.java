package ru.potapov.tm.command.terminal;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.Application;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.net.URL;
import java.util.Objects;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@NoArgsConstructor
public class AboutCommand extends AbstractCommand {
    public AboutCommand(@Nullable final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        setNeedAuthorize(false);
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Prints the info about app";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        //String version = Manifests.read("Implementation-Version");

        Class clazz = Application.class;
        String className = clazz.getSimpleName() + ".class";
        String classPath = clazz.getResource(className).toString();
        if (!classPath.startsWith("jar")) {
            // Class not from JAR
            return;
        }
        String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) +
                "/META-INF/MANIFEST.MF";
        Manifest manifest = new Manifest(new URL(manifestPath).openStream());
        Attributes attr = manifest.getMainAttributes();
        String version = attr.getValue("Implementation-Version");

        String s = "App Task-manager Build number: " + version;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getTerminalService().printlnArbitraryMassage(s);
    }
}
