package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ServiceLocator;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import ru.potapov.tm.endpoint.*;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T> {
    @Inject
    @Nullable private ServiceLocator serviceLocator;

    @Nullable private Session       session;

    @Nullable private URL           url;
    @Nullable private QName         qName;
    @Nullable private Service       service;
}
