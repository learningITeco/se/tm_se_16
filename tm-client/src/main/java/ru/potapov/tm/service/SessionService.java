package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.endpoint.*;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.net.URL;

@Getter
@Setter
//@NoArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {
    @Nullable private ISessionEndpoint webService;
    @Nullable private Session session;

    public SessionService() throws MalformedURLException {
        super();
        setUrl(new URL("http://localhost:8080/SessionEndpoint?wsdl"));
        setQName(new QName("http://endpoint.tm.potapov.ru/", "SessionEndpointService"));
        setService(getService().create(getUrl(), getQName()));
        setWebService(getService().getPort(ISessionEndpoint.class));
    }

    @Override
    public @Nullable boolean checkUserSession() {
        return false;
    }

    @Override
    @SneakyThrows
    public void removeSession(@NotNull Session session) {
        webService.removeSession(session);
    }

    @Override
    public @Nullable Session getSessionById(@NotNull String sessionId) {
        return webService.getSessionById(sessionId);
    }

    public Session getSession() {
        return session;
    }
}
