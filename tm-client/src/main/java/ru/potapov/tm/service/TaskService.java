package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.endpoint.*;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Getter
@Setter
//@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {
    @Nullable private ITaskEndpoint webService;
    
    public TaskService() throws MalformedURLException {
        super();
        setUrl(new URL("http://localhost:8080/TaskEndpoint?wsdl"));
        setQName(new QName("http://endpoint.tm.potapov.ru/", "TaskEndpointService"));
        setService(getService().create(getUrl(), getQName()));
        setWebService(getService().getPort(ITaskEndpoint.class));
    }

    public int checkSize() throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return 0;

        return webService.checkTaskSize(getSession());
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return null;

        return webService.findTaskByName(getSession(), name);
    }

    @Override
    public void remove(@NotNull final Task task) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;

        webService.removeTask(getSession(), task);
    }

    @Override
    public void removeAll(@NotNull final String userId) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;

        webService.removeAllTasksByUserId(getSession(), userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Task> listTasks) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;

        webService.removeAllTasks(getSession(), (List<Task>) listTasks);
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException_Exception, ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return task;

        return webService.renameTask(getSession(), task, name);
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException_Exception, ValidateExeption_Exception {
        if (Objects.isNull(webService))
            return;

        webService.changeProject(getSession(), task, project);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return new ArrayList<>();

        return webService.findAllTasksByUserId(getSession(), userId, projectId);
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(String userId) throws ValidateExeption_Exception {
        if (Objects.isNull(webService))
            return new ArrayList<>();

        Session session = getSession();
        User user = getServiceLocator().getUserService().getUserById(userId);
        if ( getServiceLocator().getUserService().isAdministrator(user) ){
            return webService.findAllTasks(session, "");
        }

        return webService.findAllByUser(session, userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String projectId) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return new ArrayList<>();
        return webService.findAllTasks(getSession(), projectId);
    }

    @Override
    public void put(@NotNull final Task task) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;
        webService.putTask(getSession(), task);
    }

//    @Override
//    public @NotNull Map<String, Task> getMapRepository() {
//        return (Map<String, Task>)webService.getTaskMapRepository();
//    }

//    @Override
//    public void setMapRepository(@NotNull final Map<String, Task> mapRepository){
//        webService.setTaskMapRepository(new MyMap(mapRepository) );
//    };

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Task task) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return "";

        return webService.collectTaskInfo(getSession(), task);
    }

    @Override
    public void loadBinar(@Nullable Session session) throws Exception_Exception {
        webService.loadBinar(session);
    }

    @Override
    public void saveBinar(@Nullable Session session) throws Exception_Exception {
        webService.saveBinar(session);
    }

    @Override
    public void saveJaxb(@Nullable Session session, boolean formatXml) throws Exception_Exception {
        webService.saveJaxb(session, formatXml);
    }

    @Override
    public void loadJaxb(@Nullable Session session, boolean formatXml) throws Exception_Exception {
        webService.loadJaxb(session, formatXml);
    }

    @Override
    public void saveFasterXml(@Nullable Session session) throws Exception_Exception {
        webService.saveFasterXml(session);
    }

    @Override
    public void loadFasterXml(@Nullable Session session) throws Exception_Exception {
        webService.loadFasterXml(session);
    }

    @Override
    public void saveFasterJson(@Nullable Session session) throws Exception_Exception {
        webService.saveFasterJson(session);
    }

    @Override
    public void loadFasterJson(@Nullable Session session) throws Exception_Exception {
        webService.loadFasterJson(session);
    }
}
