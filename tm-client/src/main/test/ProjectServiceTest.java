import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.potapov.tm.endpoint.Project;
import ru.potapov.tm.endpoint.Session;
import ru.potapov.tm.endpoint.Status;
import ru.potapov.tm.endpoint.User;
import ru.potapov.tm.service.ProjectService;
import ru.potapov.tm.service.SessionService;
import ru.potapov.tm.service.UserService;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

public class ProjectServiceTest {
    private ProjectService projectService;
    private UserService userService;
    private SessionService sessionService;
    private Project projectTest;
    private Project projectTest3;
    private Session session;

    @After
    public void tearDown() throws Exception {
        try {
            Project project2 = projectService.findOne("PTEST");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Project project2 = projectService.findOne("PTEST2");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Project project2 = projectService.findOne("PTEST3");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Project project2 = projectService.findOne("PTEST4");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Project project2 = projectService.findOne("PTEST6");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}

//        try {
//            if (session != null)
//                new SessionService().removeSession( session );
//        }catch (Exception e){e.printStackTrace();}
    }

    @Before
    public void setUp() throws Exception {
        projectService = new ProjectService();
        userService = new UserService();
        sessionService = new SessionService();

        //User & session
        User adminUser = null;
        session = null;
        try {
            adminUser =  userService.getUserByNamePass("test", "1");
        }catch (Exception e){e.printStackTrace();}

        session = adminUser.getSession();
        sessionService.setSession(session);
        projectService.setSession(session);
        userService.setAuthorizedUser(adminUser);
        userService.setAuthorized(true);

        //Project
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar datXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        projectTest = new Project();
        projectTest.setId("dffd-22sdsd-sdcx-23w-d");
        projectTest.setName("PTEST");
        projectTest.setDescription("D");
        projectTest.setStatus(Status.PLANNED);
        projectTest.setDateStart(datXml);
        projectTest.setDateFinish(datXml);
        projectTest.setUserId(adminUser.getId());
        projectService.put(projectTest, session);

        Project projectTest2 = new Project();
        projectTest2.setId("dffd-22sdsd-sdcx-23w-e");
        projectTest2.setName("PTEST3");
        projectTest2.setDescription("D");
        projectTest2.setStatus(Status.PLANNED);
        projectTest2.setDateStart(datXml);
        projectTest2.setDateFinish(datXml);
        projectTest2.setUserId(adminUser.getId());
        projectService.put(projectTest2, session);
        
        projectTest3 = new Project();
        projectTest3.setId("dffd-22sdsd-sdcx-23w-s");
        projectTest3.setName("PTEST4");
        projectTest3.setDescription("D");
        projectTest3.setStatus(Status.PLANNED);
        projectTest3.setDateStart(datXml);
        projectTest3.setDateFinish(datXml);
        projectTest3.setUserId(adminUser.getId());
        projectService.put(projectTest3, session);
    }

    @Test
    public void checkSize() {
        int i = 0;
        try {
            i = projectService.checkSize();
        }catch (Exception e){ e.printStackTrace();}

        Assert.assertTrue("Project repository is empty",i>0);
    }

    @Test
    public void findOne() {
        try {
            Assert.assertNotNull(projectService.findOne(projectTest.getName()));
        }catch (Exception e){ e.printStackTrace();}
    }

    @Test
    public void findOneById() {
        try {
            Assert.assertNotNull(projectService.findOneById(projectService.findProjectByName(projectTest.getName()).getId()));
        }catch (Exception e){ e.printStackTrace();}
    }

    @Test
    public void findProjectByName() {
        try {
            Assert.assertNotNull(projectService.findOne(projectTest.getName()));
        }catch (Exception e){ e.printStackTrace();}
    }

    @Test
    public void remove() {
        try {
            Project project2 = projectService.findOne("PTEST3");
            projectService.remove(project2);
            Assert.assertNull("Cannot delete", projectService.findOne("PTEST3"));
        }catch (Exception e){ e.printStackTrace();}
    }
}