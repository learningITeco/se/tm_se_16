import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.potapov.tm.endpoint.*;
import ru.potapov.tm.service.ProjectService;
import ru.potapov.tm.service.SessionService;
import ru.potapov.tm.service.TaskService;
import ru.potapov.tm.service.UserService;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.Exception;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

import static org.junit.Assert.*;

public class TaskServiceTest {
    private ProjectService projectService;
    private UserService userService;
    private SessionService sessionService;
    private TaskService taskService;
    private Task taskTest;
    private Task taskTest2;
    private Task taskTest3;
    private Project projectTest;
    private Project projectTest2;

    @Before
    public void setUp() throws Exception {
        projectService = new ProjectService();
        taskService = new TaskService();
        userService = new UserService();
        sessionService = new SessionService();

        //User & session
        User adminUser = null;
        Session session = null;
        try {
            adminUser =  userService.getUserByNamePass("test", "1");
        }catch (Exception e){e.printStackTrace();}

        session = adminUser.getSession();
        sessionService.setSession(session);
        taskService.setSession(session);
        projectService.setSession(session);
        userService.setAuthorizedUser(adminUser);
        userService.setAuthorized(true);

        try {
            if (session != null)
                new SessionService().removeSession( session );
        }catch (Exception e){e.printStackTrace();}


        //Project
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar datXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        projectTest = new Project();
        projectTest.setId("2125c21c-7a55-4b8b-9a17-b15afa7dfb94");
        projectTest.setName("PTEST");
        projectTest.setDescription("D");
        projectTest.setStatus(Status.PLANNED);
        projectTest.setDateStart(datXml);
        projectTest.setDateFinish(datXml);
        projectTest.setUserId(adminUser.getId());
        projectService.put(projectTest, session);

        projectTest2 = new Project();
        projectTest2.setId("2125c21c-7a55-4b8b-9a17-b15afa7dfb95");
        projectTest2.setName("PTEST2");
        projectTest2.setDescription("D");
        projectTest2.setStatus(Status.PLANNED);
        projectTest2.setDateStart(datXml);
        projectTest2.setDateFinish(datXml);
        projectTest2.setUserId(adminUser.getId());
        projectService.put(projectTest2, session);

        taskTest = new Task();
        taskTest.setId("2125c21c-7a55-4b8b-9a17-b15afa7dfb96");
        taskTest.setName("TTEST");
        taskTest.setProjectId(projectService.findOne(projectTest.getName()).getId());
        taskTest.setDescription("D");
        taskTest.setStatus(Status.PLANNED);
        taskTest.setDateStart(datXml);
        taskTest.setDateFinish(datXml);
        taskTest.setUserId(adminUser.getId());
        taskService.put(taskTest);

        taskTest2 = new Task();
        taskTest2.setId("2125c21c-7a55-4b8b-9a17-b15afa7dfb97");
        taskTest2.setName("TTEST2");
        taskTest2.setProjectId(projectService.findOne(projectTest.getName()).getId());
        taskTest2.setDescription("D");
        taskTest2.setStatus(Status.PLANNED);
        taskTest2.setDateStart(datXml);
        taskTest2.setDateFinish(datXml);
        taskTest2.setUserId(adminUser.getId());
        taskService.put(taskTest2);

        taskTest3 = new Task();
        taskTest3.setId("2125c21c-7a55-4b8b-9a17-b15afa7dfb98");
        taskTest3.setName("TTEST3");
        taskTest3.setProjectId(projectService.findOne(projectTest.getName()).getId());
        taskTest3.setDescription("D");
        taskTest3.setStatus(Status.PLANNED);
        taskTest3.setDateStart(datXml);
        taskTest3.setDateFinish(datXml);
        taskTest3.setUserId(adminUser.getId());
        taskService.put(taskTest3);
        taskTest = taskService.findTaskByName(taskTest.getName());
        taskTest2 = taskService.findTaskByName(taskTest2.getName());
        taskTest3 = taskService.findTaskByName(taskTest3.getName());
        projectTest = projectService.findOne(projectTest.getName());
        projectTest2 = projectService.findOne(projectTest2.getName());
    }

    @After
    public void tearDown() throws Exception {
        try {
            Project project2 = projectService.findOne("PTEST");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Project project2 = projectService.findOne("PTEST2");
            if (Objects.nonNull(project2))
                projectService.remove(project2);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Task task = taskService.findTaskByName("TTEST");
            if (Objects.nonNull(task))
                taskService.remove(task);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Task task = taskService.findTaskByName("TTEST2");
            if (Objects.nonNull(task))
                taskService.remove(task);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Task task = taskService.findTaskByName("TTEST3");
            if (Objects.nonNull(task))
                taskService.remove(task);
        }catch (Exception e){ e.printStackTrace();}
        try {
            Task task = taskService.findTaskByName("TTEST22");
            if (Objects.nonNull(task))
                taskService.remove(task);
        }catch (Exception e){ e.printStackTrace();}

    }

    @Test
    public void checkSize() {
        int i = 0;
        try {
            i = taskService.checkSize();
        }catch (Exception e){ e.printStackTrace();}

        Assert.assertTrue("Project repository is empty",i>0);
    }

    @Test
    public void findTaskByName() {
        try {
            Assert.assertNotNull(taskService.findTaskByName(taskTest.getName()));
        }catch (Exception e){ e.printStackTrace();}
    }

    @Test
    public void remove() {
        try {
            Task task = taskService.findTaskByName("TTEST3");
            taskService.remove(task);
            Assert.assertNull("Cannot delete",taskService.findTaskByName("TTEST3"));
        }catch (Exception e){ e.printStackTrace();}
    }

    @Test
    public void renameTask() {
        try {
            taskService.renameTask(taskTest2, "TTEST22");
            taskTest2 = taskService.findTaskByName("TTEST22");
            Assert.assertTrue(taskTest2.getName().equals("TTEST22"));
        }catch (Exception e){ e.printStackTrace();}
    }

    @Test
    public void changeProject() {
        try {
            taskService.changeProject(taskTest2, projectTest2);
            taskTest2 = taskService.findTaskByName(taskTest2.getName());
            Assert.assertTrue(taskTest2.getProjectId().equals(projectTest2.getId()));
        }catch (Exception e){ e.printStackTrace();}
    }

}