package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.potapov.tm.dto.Project;
import ru.potapov.tm.dto.Task;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.Collection;
import java.util.Objects;

@WebService
public interface ITaskService {
    int checkTaskSize() throws ValidateExeption;
    @Nullable Task findTaskByName(@NotNull final String name) throws ValidateExeption;
    void removeTask(@NotNull final Task task) throws ValidateExeption;
    void removeAllTasksByUserId(@NotNull final String userId) throws ValidateExeption;
    void removeAllTasks(@NotNull final Collection<Task> listTasks) throws ValidateExeption;
    @NotNull Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption;
    void changeProject(@NotNull final Task task, @NotNull final Project project) throws CloneNotSupportedException, ValidateExeption;
    @NotNull Collection<Task> findAllTasks(@NotNull final String projectId) throws ValidateExeption;
    @NotNull Collection<Task> findAllTasksByUserId(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption;
    @NotNull Collection<Task> findAllByUser(@NotNull final String userId) throws ValidateExeption;
    void putTask(@NotNull Task task) throws ValidateExeption;
    @NotNull String collectTaskInfo(@NotNull final Task task) throws ValidateExeption;
    @NotNull
    Collection<ru.potapov.tm.entity.Task> collectionDtoToEntity(@NotNull Collection<ru.potapov.tm.dto.Task> collectionTaskDto);
    @NotNull
    Collection<ru.potapov.tm.dto.Task> collectionEntityToDto(@NotNull Collection<ru.potapov.tm.entity.Task> collectionTaskEntity);
    @NotNull
    ru.potapov.tm.entity.Task dtoToEntity(@NotNull ru.potapov.tm.dto.Task taskDto);
    @Nullable
    ru.potapov.tm.dto.Task entityToDto(@Nullable ru.potapov.tm.entity.Task taskEntity);
    void loadBinar() throws Exception;
    void saveBinar() throws Exception;

    void saveJaxb(final boolean formatXml) throws Exception;
    void loadJaxb(final boolean formatXml) throws Exception;

    void saveFasterXml() throws Exception;
    void loadFasterXml() throws Exception;

    void saveFasterJson() throws Exception;
    void loadFasterJson() throws Exception;
}
