package ru.potapov.tm.bootstrap;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.*;

import ru.potapov.tm.endpoint.*;
import ru.potapov.tm.repository.*;
import ru.potapov.tm.repository.IRepository;
import ru.potapov.tm.service.*;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.xml.ws.Endpoint;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApplicationScoped
public class  Bootstrap implements Serializable , ServiceLocator {
    @Inject @NotNull private IProjectService projectService;
    @Inject @NotNull private ITaskService taskService;
    @Inject @Nullable private IUserService userService;
    @Inject @NotNull private ISessionService sessionService;
    @Inject @NotNull private ITerminalService terminalService;

    @Inject @NotNull private IProjectEndpoint projectIEndpoint;
    @Inject @NotNull private ITaskEndpoint taskIEndpoint;
    @Inject @NotNull private IUserEndpoint userIEndpoint;
    @Inject @NotNull private ISessionEndpoint sessionIEndpoint;
    @Nullable private Endpoint projectEndpoint, taskEndpoint, userEndpoint, sessionEndpoint;

    @NotNull final SimpleDateFormat ft            = new SimpleDateFormat("dd-MM-yyyy");

    public Bootstrap() throws SQLException {
    }

    public void init() throws Exception{
        if (getUserService().sizeUserMap() == 0)
            getUserService().createPredefinedUsers();
        getUserService().sizeUserMap();

        startEndpoints();

        String command = "";  Scanner in = new Scanner(System.in);
        getProjectService().checkProjectSize();
        while (!"exit".equals(command)){
            System.out.println("Commands: <stop>, <start>, <exit>");
            command = in.nextLine();

            switch (command){
                case "start": try {
                    startEndpoints();
                } catch ( Exception e) { e.printStackTrace(); }
                finally { break; }
                case "stop": stopEndpoints();break;
            }
        }
        stopEndpoints();
    }

    private void startEndpoints() {
        projectEndpoint =  Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectIEndpoint);
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");

        taskEndpoint    = Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskIEndpoint);
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");

        userEndpoint    = Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userIEndpoint);
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");

        sessionEndpoint = Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionIEndpoint);
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
        System.out.println("Command <start> completed");
    }

    private void stopEndpoints() {
        projectEndpoint.stop();
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl - has stopped");

        taskEndpoint.stop();
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl - has stopped");

        userEndpoint.stop();
        System.out.println("http://localhost:8080/UserEndpoint?wsdl - has stopped");

        sessionEndpoint.stop();
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl - has stopped");
        System.out.println("Command <stop> completed");
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Nullable
    @Inject
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService(){
        return sessionService;
    }
}