package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.dto.Session;
import ru.potapov.tm.util.ValidateExeption;

import javax.inject.Named;
import javax.jws.WebService;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@Named("ISessionEndpoint")
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractServiceEndpoint<Session> implements ISessionEndpoint {
    @NotNull final Map<String, Session> mapSession = new HashMap<>();
    @Nullable private Bootstrap bootstrap;

    public SessionEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        this.bootstrap = bootstrap;
    }

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public boolean validSession(@NotNull Session session) throws ValidateExeption {
        return getServiceLocator().getSessionService().validSession(session);
    }

    @Override
    public void addSession(Session session, String userId) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getSessionService().addSession(session, userId);
    }

    @Override
    public Session getSessionById(String sessionId) {
        return getServiceLocator().getSessionService().getSessionById(sessionId);
    }

    @Override
    public void removeSession(@NotNull Session session) throws ValidateExeption {
        getServiceLocator().getSessionService().removeSession(session);
    }
}
