package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.RoleType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "app_user")
@Setter
@Getter
@NoArgsConstructor
public class User extends AbstracEntity implements Cloneable, Serializable {

    @Id
    @Column(name = "id")
    @Nullable private String id = UUID.randomUUID().toString();

    @Nullable private String login;

    @Column(name = "passwordHash")
    @Nullable private String hashPass;

    @Column(name = "session_id")
    @Nullable private String sessionId;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    @NotNull private RoleType roleType = RoleType.User;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
