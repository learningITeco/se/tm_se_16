package ru.potapov.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository(forEntity = Project.class)
public interface IProjectRepository extends EntityRepository<Project, String> {

    //@Nullable Project findById(@NotNull final String id);

    @Nullable Project findOptionalById(@NotNull final String id);

    @NotNull List<Project> findAll();

    @NotNull List<Project> findAllByUserId(@NotNull final String userId);

    @Nullable Project findOptionalByName(@NotNull final String name);

    @Nullable Project findOptionalByNameByUserId(@NotNull final String userId, @NotNull final String name);

    Project save(@NotNull final Project project);

    void refresh(@NotNull final Project project);

    void remove(@NotNull final Project project);

    void removeAll();
}
