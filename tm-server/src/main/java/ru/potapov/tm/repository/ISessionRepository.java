package ru.potapov.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository(forEntity = Session.class)
public interface ISessionRepository extends EntityRepository<Session, String> {
    
    List<Session> findAll();

    Session findById(@NotNull final String id);

    Session findOptionalById(@NotNull final String id);

    void refresh(@NotNull final Session session);

    Session save(@NotNull final Session session) ;

    void removeAll();

    void remove(@NotNull Session session);
}
