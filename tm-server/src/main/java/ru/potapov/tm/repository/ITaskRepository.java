package ru.potapov.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Repository(forEntity = Task.class)
public interface ITaskRepository extends EntityRepository<Task, String> {

    List<Task> findAll();

    List<Task> findAllByUser(@NotNull final String userId);

    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    Task findOptionalById(@NotNull String id);

    Task findOptionalByName(@NotNull String name);

    Task findOptionalByNameAndUserId(@NotNull final String userId, @NotNull final String name);

    void remove(@NotNull final Task task) ;

    Task  save(@NotNull final Task task) ;

    void refresh(@NotNull final Task task);

    void removeAll(@NotNull  EntityManager em);
}
