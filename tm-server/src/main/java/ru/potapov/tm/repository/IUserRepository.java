package ru.potapov.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository(forEntity = User.class)
public interface IUserRepository extends EntityRepository<User, String> {
//    @Nullable
//    User findFirst (@NotNull final String login);

    @Nullable
    User findOptionalByLogin (@NotNull final String login);

    @Nullable User findById(@NotNull final String id);

    @NotNull List<User> findAll();

    User save(@NotNull User t);

    void refresh(@NotNull User t);

    void remove(@NotNull User t);

    void removeAll();
}
