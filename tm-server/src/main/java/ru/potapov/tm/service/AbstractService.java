package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.deltaspike.data.api.EntityRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.AbstracEntity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstracEntity> implements Serializable {
    @Inject @Nullable private ServiceLocator serviceLocator;
    @Inject @NotNull private EntityManager entityManager;
    //@Inject @NotNull private EntityRepository<T, String> repository;
    @Nullable private String className = "data";//((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.")[((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.").length-1];

    public AbstractService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;

    }
}
