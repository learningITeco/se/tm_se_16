package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.jpa.api.transaction.TransactionScoped;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.dto.Session;
import ru.potapov.tm.dto.User;
import ru.potapov.tm.repository.ISessionRepository;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@ApplicationScoped
@Transactional
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ISessionEndpoint")
public class SessionService extends AbstractService<Session> implements ISessionService {
    @Inject @NotNull private ISessionRepository repository;
    @NotNull final Map<String, Session> mapSession = new HashMap<>();
    @Nullable private Bootstrap bootstrap;

    public SessionService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.bootstrap = bootstrap;
    }

    @Override
    public boolean validSession(@NotNull Session sessionUser) throws ValidateExeption {
        @Nullable Session sessionUserClone;
        try { sessionUserClone = (Session)sessionUser.clone(); }catch (Exception e){return false;}
        sessionUserClone.setSignature("");
        @Nullable final String sign = SignatureUtil.sign(sessionUserClone,"",1);
        if ( sign.equals(sessionUser.getSignature()) ){
            final long timDif = (new Date().getTime() - sessionUser.getDateStamp());
            if ( timDif < 86400000 )
                return true;
        }
        return false;
    }

    @Override
    public @Nullable Session generateSession(@NotNull Session sessionUser, @NotNull User user) {
        @Nullable Session sessionUserClone;
        try { sessionUserClone = (Session)sessionUser.clone(); }catch (Exception e){return null;}
        sessionUserClone.setSignature("");
        sessionUser.setSignature(SignatureUtil.sign(sessionUserClone,"", 1));
        sessionUser.setUserId(user.getId());
        //addSession(sessionUser, null);
        return sessionUser;
    }

    @Override
    public void addSession(@NotNull Session sessionUser, @Nullable final String userId){// throws ValidateExeption {
        //getServiceLocator().getSessionService().validSession(sessionUser);
        ru.potapov.tm.entity.Session entitySession = dtoToEntity(sessionUser);
        
            getRepository().save(entitySession);
            }

    @Override
    public Session getSessionById(@Nullable final String sessionId){// throws ValidateExeption {
        //getServiceLocator().getSessionService().validSession(sessionUser);
        Session session = null;
        session = entityToDto(getRepository().findOptionalById(sessionId));
            return session;
    }

    @Override
    public @Nullable Collection<Session> getSessionCollection() {
        @Nullable Collection<Session> list = new ArrayList<>();
        list = collectionEntityToDto(getRepository().findAll());
            return list;
    }

    @Override
    public void removeSession(@NotNull Session sessionUser) {
        ru.potapov.tm.entity.Session entitySession = dtoToEntity(sessionUser);
        getRepository().remove(entitySession);
            }

    @NotNull
    @SneakyThrows
    private Collection<ru.potapov.tm.entity.Session> collectionDtoToEntity(Collection<ru.potapov.tm.dto.Session> collectionSessionDto){
        Collection<ru.potapov.tm.entity.Session> list = new ArrayList<>();
        for (ru.potapov.tm.dto.Session session : collectionSessionDto) {
            list.add(dtoToEntity(session));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    private Collection<ru.potapov.tm.dto.Session> collectionEntityToDto(Collection<ru.potapov.tm.entity.Session> collectionSessionEntity){
        Collection<ru.potapov.tm.dto.Session> list = new ArrayList<>();
        for (ru.potapov.tm.entity.Session session : collectionSessionEntity) {
            list.add(entityToDto(session));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    private ru.potapov.tm.entity.Session dtoToEntity(ru.potapov.tm.dto.Session sessionDto){
        ru.potapov.tm.entity.Session sessionEntity = new ru.potapov.tm.entity.Session();
        sessionEntity.setId(sessionDto.getId());
        sessionEntity = getRepository().findOptionalById(sessionDto.getId());
            if (Objects.isNull(sessionEntity)){
            sessionEntity = new ru.potapov.tm.entity.Session();
            sessionEntity.setId(sessionDto.getId());
            sessionEntity.setUser( getServiceLocator().getUserService().dtoToEntity(getServiceLocator().getUserService().getUserById(sessionDto.getUserId())) );
            sessionEntity.setSignature(sessionDto.getSignature());
            sessionEntity.setDateStamp(sessionDto.getDateStamp());
        }
        if (!sessionEntity.getUser().getId().equals(sessionDto.getUserId()))
            sessionEntity.setUser( getServiceLocator().getUserService().dtoToEntity(getServiceLocator().getUserService().getUserById(sessionDto.getUserId())) );
        if (!sessionEntity.getSignature().equals(sessionDto.getSignature()))
            sessionEntity.setSignature(sessionDto.getSignature());
        if (sessionEntity.getDateStamp() != sessionDto.getDateStamp())
            sessionEntity.setDateStamp(sessionDto.getDateStamp());

        return sessionEntity;
    }

    @Nullable
    @SneakyThrows
    public ru.potapov.tm.dto.Session entityToDto(ru.potapov.tm.entity.Session sessionEntity){
        if (sessionEntity == null)
            return null;

        ru.potapov.tm.dto.Session sessionDto = new Session();
        sessionDto.setId(sessionEntity.getId());

        if (Objects.nonNull(sessionEntity)){
            if (!sessionEntity.getUser().getId().equals(sessionDto.getUserId()))
                sessionDto.setUserId(sessionEntity.getUser().getId());
            if (!sessionEntity.getSignature().equals(sessionDto.getSignature()))
                sessionDto.setSignature(sessionEntity.getSignature());
            if (sessionEntity.getDateStamp() != sessionDto.getDateStamp())
                sessionDto.setDateStamp(sessionEntity.getDateStamp());

        }

        return sessionDto;
    }
}
