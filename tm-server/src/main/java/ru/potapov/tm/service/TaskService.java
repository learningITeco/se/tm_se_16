package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.Project;
import ru.potapov.tm.dto.Task;
import ru.potapov.tm.repository.ITaskRepository;
import ru.potapov.tm.util.ValidateExeption;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@ApplicationScoped
@Transactional
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ITaskEndpoint")
public class TaskService extends AbstractService<Task> implements ITaskService {
    @Inject @NotNull private ITaskRepository repository;
    public TaskService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }
    
    @Override
    public int checkTaskSize() throws ValidateExeption {
        int i = 0;
        i = getRepository().findAll().size();
        return i;
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name) throws ValidateExeption {
        @Nullable Task task = null;
        task = entityToDto(getRepository().findOptionalByName(name));
        return task;
    }

    @Override
    public void removeTask(@NotNull final Task task) throws ValidateExeption {
        getRepository().remove(getRepository().findOptionalById(task.getId()));
    }

    @Override
    public void removeAllTasksByUserId(@NotNull final String userId) throws ValidateExeption {
        for (ru.potapov.tm.entity.Task task : getRepository().findAllByUser(userId)) {
            if (userId.equals(task.getUser().getId()))
                getRepository().remove(task);
        }
    }

    @Override
    public void removeAllTasks(@NotNull final Collection<Task> listTasks) throws ValidateExeption {
        for (Task task : listTasks) {
            getRepository().remove(getRepository().findOptionalById(task.getId()));
        }
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption {
        if (Objects.nonNull(name)) {
            @NotNull Task newTask = (Task) task.clone();
            newTask.setName(name);
            newTask.setId(task.getId());
            getRepository().save(dtoToEntity(newTask));
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException, ValidateExeption {
        if (Objects.nonNull(project)) {
            @NotNull Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            newTask.setId(task.getId());
            getRepository().save(dtoToEntity(newTask));
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasksByUserId(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption {
        @NotNull Collection<Task> list = new ArrayList<>();
        list = collectionEntityToDto(getRepository().findAllByUserIdAndProjectId(userId, projectId));
        return list;
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(String userId) throws ValidateExeption {
        @NotNull Collection<Task> list = new ArrayList<>();
        list = collectionEntityToDto(getRepository().findAllByUser(userId));
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasks(@NotNull final String projectId) throws ValidateExeption {
        @NotNull Collection<Task> list = new ArrayList<>();
        list = collectionEntityToDto(getRepository().findAll());
        return list;
    }

    @Override
    public void putTask(@NotNull final Task task) throws ValidateExeption {
        ru.potapov.tm.entity.Task entityTask = dtoToEntity(task);

        if (Objects.isNull(getRepository().findOptionalById(task.getId())))
            getRepository().save(entityTask);
        else
            getRepository().save(entityTask);
    }

    @Override
    public void loadBinar() throws Exception {
    }

    @Override
    public void saveBinar() throws Exception {
    }

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Task task) throws ValidateExeption {
        @NotNull String res = "";
        if (Objects.isNull(task.getDateStart()) || Objects.isNull(task.getDateFinish()) || Objects.isNull(getServiceLocator())
        )
            return res;

        res += "\n";
        res += "    Task [" + task.getName() + "]" + "\n";
        res += "    Project " + getServiceLocator().getProjectService().findOneProjectById(task.getProjectId()) + "]" + "\n";
        res += "    Status: " + task.getStatus() + "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " + getServiceLocator().getFt().format(task.getDateStart()) + "\n";
        res += "    Date finish: " + getServiceLocator().getFt().format(task.getDateFinish()) + "\n";

        return res;
    }

    @NotNull
    @SneakyThrows
    public Collection<ru.potapov.tm.entity.Task> collectionDtoToEntity
            (@NotNull Collection<ru.potapov.tm.dto.Task> collectionTaskDto) {
        Collection<ru.potapov.tm.entity.Task> list = new ArrayList<>();
        for (ru.potapov.tm.dto.Task task : collectionTaskDto) {
            list.add(dtoToEntity(task));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<ru.potapov.tm.dto.Task> collectionEntityToDto
            (@NotNull Collection<ru.potapov.tm.entity.Task> collectionTaskEntity) {
        Collection<ru.potapov.tm.dto.Task> list = new ArrayList<>();
        for (ru.potapov.tm.entity.Task task : collectionTaskEntity) {
            list.add(entityToDto(task));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public ru.potapov.tm.entity.Task dtoToEntity(@NotNull ru.potapov.tm.dto.Task taskDto) {
        ru.potapov.tm.entity.Task taskEntity = new ru.potapov.tm.entity.Task();
        taskEntity.setId(taskDto.getId());

        taskEntity = getRepository().findOptionalById(taskDto.getId());


        if (Objects.isNull(taskEntity)) {
            taskEntity = new ru.potapov.tm.entity.Task();
            taskEntity.setId(taskDto.getId());
            taskEntity = new ru.potapov.tm.entity.Task();
            taskEntity.setDescription(taskDto.getDescription());
            taskEntity.setProject(getServiceLocator().getProjectService().dtoToEntity(getServiceLocator().getProjectService().findOneProjectById(taskDto.getProjectId())));
            taskEntity.setName(taskDto.getName());
            taskEntity.setStatus(taskDto.getStatus());
            taskEntity.setUser(getServiceLocator().getUserService().dtoToEntity(getServiceLocator().getUserService().getUserById(taskDto.getUserId())));
            taskEntity.setDateStart(taskDto.getDateStart());
            taskEntity.setDateFinish(taskDto.getDateFinish());
        }
        if (!taskEntity.getDescription().equals(taskDto.getDescription()))
            taskEntity.setDescription(taskDto.getDescription());
        if (!taskEntity.getProject().getId().equals(taskDto.getProjectId()))
            taskEntity.setProject(getServiceLocator().getProjectService().dtoToEntity(getServiceLocator().getProjectService().findOneProjectById(taskDto.getProjectId())));
        if (!taskEntity.getName().equals(taskDto.getName()))
            taskEntity.setName(taskDto.getName());
        if (!taskEntity.getStatus().equals(taskDto.getStatus()))
            taskEntity.setStatus(taskDto.getStatus());
        if (!taskEntity.getUser().getId().equals(taskDto.getUserId()))
            taskEntity.setUser(getServiceLocator().getUserService().dtoToEntity(getServiceLocator().getUserService().getUserById(taskDto.getUserId())));
        if (!taskEntity.getDateStart().equals(taskDto.getDateStart()))
            taskEntity.setDateStart(taskDto.getDateStart());
        if (!taskEntity.getDateFinish().equals(taskDto.getDateFinish()))
            taskEntity.setDateFinish(taskDto.getDateFinish());

        return taskEntity;
    }

    @Nullable
    @SneakyThrows
    public ru.potapov.tm.dto.Task entityToDto(@Nullable ru.potapov.tm.entity.Task taskEntity) {
        if (taskEntity == null)
            return null;

        ru.potapov.tm.dto.Task taskDto = new Task();
        taskDto.setId(taskEntity.getId());

        if (Objects.nonNull(taskEntity)) {
            if (!taskEntity.getDescription().equals(taskDto.getDescription()))
                taskDto.setDescription(taskEntity.getDescription());
            if (!taskEntity.getProject().getId().equals(taskDto.getProjectId()))
                taskDto.setProjectId(taskEntity.getProject().getId());
            if (!taskEntity.getName().equals(taskDto.getName()))
                taskDto.setName(taskEntity.getName());
            if (!taskEntity.getStatus().equals(taskDto.getStatus()))
                taskDto.setStatus(taskEntity.getStatus());
            if (!taskEntity.getUser().getId().equals(taskDto.getUserId()))
                taskDto.setUserId(taskEntity.getUser().getId());
            if (!taskEntity.getDateStart().equals(taskDto.getDateStart()))
                taskDto.setDateStart(taskEntity.getDateStart());
            if (!taskEntity.getDateFinish().equals(taskDto.getDateFinish()))
                taskDto.setDateFinish(taskEntity.getDateFinish());
        }

        return taskDto;
    }

    //Save-Load
    @Override
    public void saveJaxb(boolean formatXml) throws Exception {
    }

    @Override
    public void loadJaxb(boolean formatXml) throws Exception {
    }

    @Override
    public void saveFasterXml() throws Exception {
    }

    @Override
    public void loadFasterXml() throws Exception {
    }

    @Override
    public void saveFasterJson() throws Exception {
    }

    @Override
    public void loadFasterJson() throws Exception {
    }
}
