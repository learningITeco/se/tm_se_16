package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.RoleType;
import ru.potapov.tm.dto.Session;
import ru.potapov.tm.dto.User;
import ru.potapov.tm.repository.IUserRepository;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@ApplicationScoped
@Transactional
@WebService(endpointInterface = "ru.potapov.tm.endpoint.IUserEndpoint")
public class UserService extends AbstractService<ru.potapov.tm.entity.User> implements IUserService {
    @Inject @NotNull private IUserRepository repository;
    @Nullable  private User authorizedUser = null;
    private boolean isAuthorized = false;

    public UserService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable RoleType getUserRole(@NotNull User user) throws ValidateExeption {
        @Nullable RoleType roleType = RoleType.User;
        roleType = user.getRoleType();
        return roleType;
    }

    @Override
    public boolean isAdministrator(@NotNull User user) throws ValidateExeption {
        if (getUserRole(user).equals(RoleType.Administrator))
            return true;
        return false;
    }

    @Override
    public @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception {
        @NotNull User user = getUserByName(name);
        if (Objects.isNull(user)) {
            return null;
        }

        @NotNull final String hashPass = SignatureUtil.sign(pass, "", 1);

        if (!isUserPassCorrect(user, hashPass)) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed the user password!");
            return null;
        }

        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        getServiceLocator().getSessionService().generateSession(session, user);
        getServiceLocator().getSessionService().addSession(session, user.getId());
        user.setSession(session);
        return user;
    }

    @Override
    public int sizeUserMap() {
        return getUserCollection().size();
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role) {
        @NotNull User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        Session session = new Session();
        user.setSession(getServiceLocator().getSessionService().generateSession(session,user));

        ru.potapov.tm.entity.User userEntity = dtoToEntity(user);
        getRepository().save( userEntity);
        return user;
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name) {
        @Nullable User user = null;
        user = entityToDto(getRepository().findOptionalByLogin(name));
        return user;
    }

    @Nullable
    @Override
    public User getUserById(@Nullable final String id) throws ValidateExeption {
        @Nullable User user = null;
        user =  entityToDto(getRepository().findById(id));
        return user;
    }

    @Override
    public boolean isUserPassCorrect(@Nullable final User user, @Nullable final String hashPass) {
        boolean res = false;
        if (Objects.nonNull(user) && user.getHashPass().equals(hashPass))
            res = true;
        return res;
    }

    @NotNull
    @Override
    public Collection<User> getUserCollection() {
        @NotNull Collection<User> list = new ArrayList<>();
        list = collectionEntityToDto(getRepository().findAll());
        return list;
    }

    @NotNull
    @Override
    public User changePass(@Nullable User user, @Nullable final String newHashPass) throws CloneNotSupportedException, ValidateExeption {
        @NotNull User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        getRepository().refresh(dtoToEntity(newUser));
        return user;
    }

    @Override
    public void putUser(@Nullable final User user) throws ValidateExeption {
       getRepository().save(dtoToEntity(user));
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable final User user) throws ValidateExeption {
        @NotNull String res = "";
        res += "\n";
        res += "    User [" + user.getLogin() + "]" + "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    @Override
    public void createPredefinedUsers() {
        @NotNull String hashPass;

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user2", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("2", "", 1);
        createUser("admin", hashPass, RoleType.Administrator);

        hashPass = SignatureUtil.sign("test", "", 1);
        createUser("test", hashPass, RoleType.Administrator);
    }

    @NotNull
    @SneakyThrows
    private Collection<ru.potapov.tm.entity.User> collectionDtoToEntity(Collection<ru.potapov.tm.dto.User> collectionUserDto){
        Collection<ru.potapov.tm.entity.User> list = new ArrayList<>();
        for (ru.potapov.tm.dto.User user : collectionUserDto) {
            list.add(dtoToEntity(user));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    private Collection<ru.potapov.tm.dto.User> collectionEntityToDto(Collection<ru.potapov.tm.entity.User> collectionUserEntity){
        Collection<ru.potapov.tm.dto.User> list = new ArrayList<>();
        for (ru.potapov.tm.entity.User user : collectionUserEntity) {
            list.add(entityToDto(user));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public ru.potapov.tm.entity.User dtoToEntity(ru.potapov.tm.dto.User userDto){
        ru.potapov.tm.entity.User userEntity = new ru.potapov.tm.entity.User();
        userEntity.setId(userDto.getId());
        userEntity = getRepository().findById(userDto.getId());
        if (Objects.isNull(userEntity)){
            userEntity = new ru.potapov.tm.entity.User();
            userEntity.setId(userDto.getId());
            userEntity = new ru.potapov.tm.entity.User();
            userEntity.setLogin( userDto.getLogin() );
            userEntity.setHashPass(userDto.getHashPass());
            if (Objects.nonNull(userDto.getSession()))
                userEntity.setSessionId(userDto.getSession().getId());
            userEntity.setRoleType(userDto.getRoleType());
        }

        if (Objects.nonNull(userEntity.getLogin()) && !userEntity.getLogin().equals(userDto.getLogin()))
            userEntity.setLogin( userDto.getLogin() );
        if (Objects.nonNull(userEntity.getHashPass()) && !userEntity.getHashPass().equals(userDto.getHashPass()))
            userEntity.setHashPass(userDto.getHashPass());
        if (userDto.getSession() !=null)
            if (!userEntity.getSessionId().equals(userDto.getSession().getId()));
                userEntity.setSessionId(userDto.getId());
        if (Objects.nonNull(userEntity.getRoleType()) && !userEntity.getRoleType().equals(userDto.getRoleType()));
            userEntity.setRoleType(userDto.getRoleType());

        return userEntity;
    }

    @Nullable
    @SneakyThrows
    private ru.potapov.tm.dto.User entityToDto(ru.potapov.tm.entity.User userEntity){
        if (userEntity == null)
            return null;

        ru.potapov.tm.dto.User userDto = new User();
        userDto.setId(userEntity.getId());

        if (Objects.nonNull(userEntity)){
            if (!userEntity.getLogin().equals(userDto.getLogin()))
                userDto.setLogin(userEntity.getLogin());
            if (!userEntity.getHashPass().equals(userDto.getHashPass()))
                userDto.setHashPass(userEntity.getHashPass());
            if (Objects.nonNull(userDto.getSession()) && userEntity.getSessionId() != userDto.getSession().getId())
                userDto.setSession(getServiceLocator().getSessionService().getSessionById(userEntity.getSessionId()));
            if (!userEntity.getRoleType().equals(userDto.getRoleType()))
                userDto.setRoleType(userEntity.getRoleType());
        }

        return userDto;
    }
}
